package com.practicalProject.test.service;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.ClientPolicy;
import com.practicalProject.test.model.Policy;
import com.practicalProject.test.repositoty.ClientPolicyRepository;

import java.util.Date;
import java.util.List;

public class ClientPolicyService {

    ClientPolicyRepository clientPolicyRepository = new ClientPolicyRepository();

    public void addClientPolicy(Client client, Policy policy, String policyNumber, int period, Date startDate, Date endDate, int fee) {
        ClientPolicy clientPolicy = new ClientPolicy();
        clientPolicy.setClient(client);
        clientPolicy.setPolicy(policy);
        clientPolicy.setPolicyNumber(policyNumber);
        clientPolicy.setPeriod(period);
        clientPolicy.setStartDate(startDate);
        clientPolicy.setEndDate(endDate);
        clientPolicy.setFee(fee);
        clientPolicyRepository.addPolicy(clientPolicy);

    }

    public void updateClientPolicy(ClientPolicy clientPolicy,String policyNumber,int period,Date startDate,Date endDate,int fee){
        clientPolicy.setPolicyNumber(policyNumber);
        clientPolicy.setPeriod(period);
        clientPolicy.setStartDate(startDate);
        clientPolicy.setEndDate(endDate);
        clientPolicy.setFee(fee);
        clientPolicyRepository.updateClientPolicy(clientPolicy);
    }

    public List<ClientPolicy> getAllClientPolicy() {
        List<ClientPolicy> clientPolicyList = clientPolicyRepository.getALLClientPolicy();
        return clientPolicyList;
    }

    public List<ClientPolicy> getPoliciesByClient(Client client) {
        List<ClientPolicy> clientPolicyList = clientPolicyRepository.getPolicyByClient(client.getClientId());

        return clientPolicyList;
    }


}
