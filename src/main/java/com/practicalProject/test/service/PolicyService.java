package com.practicalProject.test.service;

import com.practicalProject.test.model.Policy;
import com.practicalProject.test.repositoty.PolicyRepository;

import java.util.ArrayList;
import java.util.List;

public class PolicyService {
    PolicyRepository policyRepository = new PolicyRepository();

    public void addPolicy(String policyName){
        Policy policy = new Policy();
        policy.setPolicyName(policyName);
        policyRepository.addPolicy(policy);
    }

    public void updatePolicy(Policy policy,String policyName){
        policy.setPolicyName(policyName);
        policyRepository.updatePolicy(policy);
    }

    public Policy getPolicyByName(String policyName){
        Policy policy = policyRepository.getPolicyByName(policyName);

        return policy;
    }

    public List<String> getPolicyNames(){
       List<Policy> policyList = policyRepository.getAllPolicies();
       List<String> policyNames = new ArrayList<>();
       for (Policy policy:policyList){
           policyNames.add(policy.getPolicyName());
       }
       return policyNames;
    }
}
