package com.practicalProject.test.view;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.repositoty.ReminderRepository;
import com.practicalProject.test.service.ClientService;
import com.practicalProject.test.util.CheckReminderDate;
import com.practicalProject.test.util.SendEmailToClient;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import javax.mail.MessagingException;
import java.util.List;


public class MainView extends Application {
    private ClientService clientService = new ClientService();

    private ObservableList<Client> clientObservableList = FXCollections.observableList(clientService.getAllClients());


    public static void main(String[] args) {

        launch();

    }
    public void init() throws MessagingException {
        List<Client> clientList = clientService.getClientWithBirthday();
        for (Client client:clientList){
            try {
                SendEmailToClient.sendMail(client.getEmailAddress());
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        ReminderRepository reminderRepository = new ReminderRepository();
        CheckReminderDate.checkReminderDate(reminderRepository.getReminders());
    }

    @Override
    public void start(Stage stage) {

        BorderPane mainPage = new BorderPane();
        //mainPage.setStyle("-fx-background-color: #2490a6;");
        stage.setTitle("Table View Sample");
        stage.setWidth(1300);
        stage.setHeight(1000);
        mainPage.setMaxHeight(540);
        mainPage.setMaxWidth(749);


        Label cnpLabel = new Label("CNP:");
        cnpLabel.setFont(new Font("Arial", 20));
        TextField cnpTextField = new TextField();
        Label nameLabel = new Label("Name:");
        nameLabel.setFont(new Font("Arial", 20));
        TextField nameTextField = new TextField();
        Label policyLabel = new Label("Policy:");
        policyLabel.setFont(new Font("Arial", 20));
        TextField policyTextFiled = new TextField();
        Label leadLabel = new Label("Lead");
        leadLabel.setFont(new Font("Arial",20));
        CheckBox leadCheckBox = new CheckBox();
        leadCheckBox.setPrefSize(50,50);
        TableView<Client> table = createTable();
        //table.setStyle("-fx-background-color: #a06fd1;");
        table.setEditable(false);
        table.setPadding(new Insets(0, 0, 0, 10));
        table.setMaxWidth(1017);
        table.setMaxHeight(800);



        //Create Buttons
        Button addButton = new Button("Add Client");
        addButton.setPrefWidth(150);
        addButton.setPrefHeight(100);
        Button updateClientButton = new Button("Update Client");
        updateClientButton.setPrefWidth(150);
        updateClientButton.setPrefHeight(100);
        Button showPoliciesButton = new Button("Show Policies");
        showPoliciesButton.setPrefWidth(150);
        showPoliciesButton.setPrefHeight(100);
        Button showReminderButton = new Button("Show Reminders");
        showReminderButton.setPrefWidth(150);
        showReminderButton.setPrefHeight(100);
        Button searchButton = new Button("Search");
        searchButton.setPrefWidth(80);
        searchButton.setPrefHeight(40);
        Button birthDayButton = new Button("Birthday Email");
        birthDayButton.setPrefWidth(80);
        birthDayButton.setPrefHeight(40);

        final HBox searchHBox = new HBox();
        searchHBox.setSpacing(10);
        searchHBox.setPadding(new Insets(50, 0, 0, 160));
        searchHBox.setAlignment(Pos.CENTER);

        searchHBox.getChildren().addAll(cnpLabel, cnpTextField, nameLabel, nameTextField, policyLabel, policyTextFiled,leadLabel,leadCheckBox, searchButton);
        mainPage.setTop(searchHBox);

        final VBox functionVBox = new VBox();
        functionVBox.setSpacing(20);
        functionVBox.setPadding(new Insets(190, 0, 0, 65));
        functionVBox.getChildren().addAll(addButton, updateClientButton, showPoliciesButton, showReminderButton,birthDayButton);
        mainPage.setLeft(functionVBox);

        mainPage.setCenter(table);


        addButton.setOnAction(event -> {
            AddClientView addClientView = new AddClientView();
            try {
                addClientView.addClient();
            } catch (Exception e) {
                e.printStackTrace();
            }
            addButton.setOnMouseReleased(event1 -> table.setItems(refreshObservebleList()));
        });

        table.setOnMouseClicked(event -> {
            Client client = table.getSelectionModel().getSelectedItem();
            updateClientButton.setOnMouseReleased(event1 -> {
                UpdateClientView clientView = new UpdateClientView();
                clientView.update(client);

                table.setItems(refreshObservebleList());

            });
            showPoliciesButton.setOnAction(event2 -> {
                ClientPoliciesView clientPoliciesView = new ClientPoliciesView();
                clientPoliciesView.ViewClientPolicies(client);
                showPoliciesButton.setOnMouseReleased(event1 -> table.setItems(refreshObservebleList()) );
            });
            showReminderButton.setOnAction(event1 -> {
                RemindersView remindersView = new RemindersView();
                remindersView.showReminders(client);
                showReminderButton.setOnMouseReleased(event2 -> table.setItems(refreshObservebleList()));
            });

        });
        Scene primaryScene = new Scene(mainPage);
        searchButton.setOnAction(event -> {

            if (cnpTextField.getText().equals("") && nameTextField.getText().equals("")) {
                if (leadCheckBox.isSelected()){
                    clientObservableList = FXCollections.observableList(clientService.getLeadsClients());
                    table.setItems(clientObservableList);
                }else {
                table.setItems(refreshObservebleList());}
            } else {
                clientObservableList = FXCollections.observableList(clientService.getClientByCNP(cnpTextField.getText(), nameTextField.getText()));
                table.setItems(clientObservableList);
            }
            searchButton.setOnMouseReleased(event1 -> {
                cnpTextField.setText("");
                nameTextField.setText("");
                policyTextFiled.setText("");
            });
        });
        birthDayButton.setOnAction(event -> {
            List<Client> clientList = clientService.getClientWithBirthday();
            for (Client client:clientList){
                try {
                    SendEmailToClient.sendMail(client.getEmailAddress());
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        });


        stage.setScene(primaryScene);
        stage.show();


    }

    private TableView createTable() {
        TableView table = new TableView();
        //Create Table Column
        TableColumn<Client, String> nameCol = new TableColumn<>("Name");
        nameCol.setMinWidth(150);
        TableColumn<Client, String> addressCol = new TableColumn<>("Address");
        addressCol.setMinWidth(200);
        TableColumn<Client, String> phoneNumberCol = new TableColumn<>("Phone Number");
        phoneNumberCol.setMinWidth(120);
        phoneNumberCol.setEditable(true);
        TableColumn<Client, String> emailCol = new TableColumn<>("Email");
        emailCol.setMinWidth(200);
        TableColumn<Client, String> cnpCol = new TableColumn<>("CNP");
        cnpCol.setMinWidth(150);
        TableColumn<Client, Boolean> gdprCol = new TableColumn<>("GDPR");
        gdprCol.setMinWidth(60);
        TableColumn<Client, Integer> reminderCol = new TableColumn<>("REM");
        reminderCol.setMinWidth(15);
        TableColumn<Client, Integer> policyNumber = new TableColumn<>("Policy");
        reminderCol.setMinWidth(15);

        //Table Data Insert
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        addressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        phoneNumberCol.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        emailCol.setCellValueFactory(new PropertyValueFactory<>("emailAddress"));
        cnpCol.setCellValueFactory(new PropertyValueFactory<>("cnp"));
        gdprCol.setCellValueFactory(new PropertyValueFactory<>("gdpr"));
        reminderCol.setCellValueFactory(new PropertyValueFactory<>("reminderList"));
        policyNumber.setCellValueFactory(new PropertyValueFactory<>("policyList"));

        table.setItems(clientObservableList);
        table.getColumns().addAll(nameCol, addressCol, phoneNumberCol, emailCol, cnpCol, gdprCol, policyNumber, reminderCol);

        return table;
    }


    private ObservableList<Client> refreshObservebleList(){
        return FXCollections.observableList(clientService.getAllClients());



    }


}
