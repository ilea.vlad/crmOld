package com.practicalProject.test.view;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.ClientPolicy;
import com.practicalProject.test.model.Policy;
import com.practicalProject.test.service.ClientPolicyService;
import com.practicalProject.test.service.ClientService;
import com.practicalProject.test.service.PolicyService;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class AddPolicyView {

    PolicyService policyService = new PolicyService();
    ClientService clientService = new ClientService();
    ClientPolicyService clientPolicyService = new ClientPolicyService();


    public void addPolicy(Client client) throws Exception {
        Stage addPolicyWindow = new Stage();
        addPolicyWindow.initModality(Modality.APPLICATION_MODAL);
        addPolicyWindow.setWidth(400);
        addPolicyWindow.setHeight(700);


        final Label policyNumberLabel = new Label("Policy Number:");
        TextField policyNumberTextField = new TextField();
        VBox numberVbox = new VBox();

        numberVbox.setAlignment(Pos.CENTER_LEFT);
        numberVbox.getChildren().addAll(policyNumberLabel, policyNumberTextField);

        final Label periodLabel = new Label("Period:");
        TextField periodTextField = new TextField();
        VBox periodVBox = new VBox();

        numberVbox.setAlignment(Pos.CENTER_LEFT);
        periodVBox.getChildren().addAll(periodLabel, periodTextField);

        final Label startDateLabel = new Label("Start Date:");
        DatePicker startDatePicker = new DatePicker();
        VBox startDateVbox = new VBox();

        startDateVbox.setAlignment(Pos.CENTER_LEFT);
        startDateVbox.getChildren().addAll(startDateLabel, startDatePicker);

        final Label endDateLabel = new Label("End Date:");
        DatePicker endDatePicker = new DatePicker();
        VBox endDateVBox = new VBox();

        endDateVBox.setAlignment(Pos.CENTER_LEFT);
        endDateVBox.getChildren().addAll(endDateLabel, endDatePicker);

        final Label feeLabel = new Label("Fee:");
        TextField feeTextField = new TextField();
        VBox feeVBox = new VBox();
        feeVBox.getChildren().addAll(feeLabel, feeTextField);

        Button addButton = new Button("Add");


        final ComboBox<String> policyComboBox = new ComboBox<>();
        for (String name : policyService.getPolicyNames()) {
            policyComboBox.getItems().add(name);
        }

        policyComboBox.setOnMouseClicked(event -> {
            policyComboBox.setOnAction(event1 -> {
                Policy policy = policyService.getPolicyByName(policyComboBox.getSelectionModel().getSelectedItem());
                addButton.setOnMousePressed(event2 -> {
                    clientPolicyService.addClientPolicy(client, policy, policyNumberTextField.getText(), Integer.parseInt(periodTextField.getText()), java.sql.Date.valueOf(startDatePicker.getValue()), java.sql.Date.valueOf(endDatePicker.getValue()), Integer.parseInt(feeTextField.getText()));

                });

            });
        });
        addButton.setOnMouseReleased(event2 -> addPolicyWindow.close());


        VBox addPolicyVBox = new VBox();
        addPolicyVBox.setSpacing(35);
        addPolicyVBox.setPadding(new Insets(30, 30, 20, 30));
        addPolicyVBox.setAlignment(Pos.CENTER);
        addPolicyVBox.getChildren().addAll(policyComboBox, numberVbox, periodVBox, startDateVbox, endDateVBox, feeVBox, addButton);
        Scene addPolicyScene = new Scene(addPolicyVBox);
        addPolicyWindow.setScene(addPolicyScene);
        addPolicyWindow.showAndWait();


    }


}
