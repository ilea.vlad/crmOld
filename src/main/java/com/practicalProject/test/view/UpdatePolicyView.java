package com.practicalProject.test.view;

import com.practicalProject.test.model.ClientPolicy;
import com.practicalProject.test.model.Policy;
import com.practicalProject.test.service.ClientPolicyService;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.swing.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class UpdatePolicyView {
    ClientPolicyService clientPolicyService = new ClientPolicyService();


    public void updatePolicy(ClientPolicy clientPolicy){
        Stage updatePolicyWindow = new Stage();
        updatePolicyWindow.initModality(Modality.APPLICATION_MODAL);
        updatePolicyWindow.setWidth(400);
        updatePolicyWindow.setHeight(700);

        final Label policyName = new Label(clientPolicy.getPolicy());

        final Label policyNumberLabel = new Label("Policy Number:");
        TextField policyNumberTextField = new TextField();
        policyNumberTextField.setText(clientPolicy.getPolicyNumber());
        VBox numberVbox = new VBox();

        numberVbox.setAlignment(Pos.CENTER_LEFT);
        numberVbox.getChildren().addAll(policyNumberLabel,policyNumberTextField);

        final Label periodLabel = new Label("Period:");
        TextField periodTextField = new TextField();
        periodTextField.setText(clientPolicy.getPeriod().toString());
        VBox periodVBox = new VBox();

        numberVbox.setAlignment(Pos.CENTER_LEFT);
        periodVBox.getChildren().addAll(periodLabel,periodTextField);

        final Label startDateLabel = new Label("Start Date:");
        TextField starDateTextField = new TextField();
        starDateTextField.setText(returnDateToString(clientPolicy.getStartDate()));
        VBox startDateVbox = new VBox();

        startDateVbox.setAlignment(Pos.CENTER_LEFT);
        startDateVbox.getChildren().addAll(startDateLabel,starDateTextField);

        final Label endDateLabel = new Label("End Date:");
        TextField endDateTextFiled = new TextField();
        endDateTextFiled.setText(returnDateToString(clientPolicy.getEndDate()));

        VBox endDateVBox = new VBox();

        endDateVBox.setAlignment(Pos.CENTER_LEFT);
        endDateVBox.getChildren().addAll(endDateLabel,endDateTextFiled);

        final Label feeLabel = new Label("Fee:");
        TextField feeTextField = new TextField();
        feeTextField.setText(clientPolicy.getFee().toString());
        VBox feeVBox = new VBox();
        feeVBox.getChildren().addAll(feeLabel,feeTextField);

        Button updateButton = new Button("Update");
        updateButton.setOnAction(event -> {
            try {
                clientPolicyService.updateClientPolicy(clientPolicy,policyNumberTextField.getText(),Integer.parseInt(periodTextField.getText()),stringToDate(starDateTextField.getText()),stringToDate(endDateTextFiled.getText()),Integer.parseInt(feeTextField.getText()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            updateButton.setOnMouseReleased(event1 -> {
                updatePolicyWindow.close();
            });
        });



        VBox updatePolicyVBox =new VBox();
        updatePolicyVBox.setSpacing(35);
        updatePolicyVBox.setPadding(new Insets(30,30,20,30));
        updatePolicyVBox.setAlignment(Pos.CENTER);
        updatePolicyVBox.getChildren().addAll(policyName,numberVbox,periodVBox,startDateVbox,endDateVBox,feeVBox,updateButton);
        Scene updatePolicyScene = new Scene(updatePolicyVBox);

        updatePolicyWindow.setScene(updatePolicyScene);
        updatePolicyWindow.showAndWait();
    }
    public String returnDateToString(Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String string = simpleDateFormat.format(date);
        return string;
    }

    public Date stringToDate(String string) throws ParseException {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(string);
        return date;
    }
}
