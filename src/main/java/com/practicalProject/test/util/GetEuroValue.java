package com.practicalProject.test.util;

public class GetEuroValue {
    public static double getValue() throws IOException {
        URL oracle = new URL("http://www.infovalutar.ro/bnr/azi/eur");
        URLConnection yc = oracle.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                yc.getInputStream()));

        return Double.valueOf(in.readLine());
    }
}
