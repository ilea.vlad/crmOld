package com.practicalProject.test.util;

import com.practicalProject.test.model.Reminder;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

public class SendReminderEmail {
    public static void sendMail(List<Reminder> reminderList) throws MessagingException {
        Properties properties = new Properties();
        String recipient = "ilea_vladutz@yahoo.com";
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        String myAccountEmail = "service.email.crm@gmail.com";
        String password = "ParolaEmailServiceCrm";
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(myAccountEmail, password);
            }
        });

        Message message = prepareMessage(session, myAccountEmail, recipient,reminderList);

        Transport.send(message);

    }

    private static Message prepareMessage(Session session, String myAccountEmail, String recipient,List<Reminder> reminderList) throws MessagingException {
        Message message = new MimeMessage(session);

        String finalMessage = "";
        message.setFrom(new InternetAddress(myAccountEmail));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        message.setSubject("Reminder");
        for (Reminder reminder : reminderList) {
            finalMessage = finalMessage + reminder.getClientId().getName() + ": " + reminder.getMessage() + "\n" + reminder.getClientId().getPhoneNumber() + "\n";
        }
        message.setText(finalMessage);
        return message;
    }
}
