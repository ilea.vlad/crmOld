package com.practicalProject.test.util;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.Reminder;
import com.practicalProject.test.repositoty.ReminderRepository;

import javax.mail.MessagingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CheckReminderDate {
    public static void checkReminderDate(List<Reminder> reminderList) throws MessagingException {
        ReminderRepository reminderRepository = new ReminderRepository();
        List<Reminder> checkedReminders = new ArrayList<>();
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd.MM");
        for (Reminder reminder : reminderList) {
            if (ft.format(date).equals(ft.format(reminder.getReminderDate())) || (ft.format(date).compareTo(ft.format(reminder.getReminderDate()))) < 0){
                checkedReminders.add(reminder);
                //reminderRepository.deleteReminder(reminder);
            }
        }

        SendReminderEmail.sendMail(checkedReminders);

    }
}
