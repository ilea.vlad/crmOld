package com.practicalProject.test;


import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.ClientPolicy;
import com.practicalProject.test.model.Policy;
import com.practicalProject.test.model.Reminder;
import com.practicalProject.test.repositoty.ClientPolicyRepository;
import com.practicalProject.test.repositoty.ClientRepository;
import com.practicalProject.test.repositoty.PolicyRepository;
import com.practicalProject.test.repositoty.ReminderRepository;
import com.practicalProject.test.service.ClientService;
import com.practicalProject.test.service.ReminderService;
import com.practicalProject.test.util.CheckReminderDate;
import com.practicalProject.test.util.CnpToBirthDate;

import javax.security.auth.login.Configuration;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.SimpleFormatter;

public class App {

    public static void main(String[] args) throws Exception {
    ReminderRepository reminderRepository = new ReminderRepository();
        CheckReminderDate.checkReminderDate(reminderRepository.getReminders());
}}