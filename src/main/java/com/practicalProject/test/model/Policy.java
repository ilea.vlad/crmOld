package com.practicalProject.test.model;

import javax.persistence.*;

@Entity
@Table(name = "policy")
public class Policy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "policy_id")
    private int policyId;

    @Column(name = "policy_type")
    private String policyName;

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }


    public String getPolicyName() {
        return policyName;
    }

    @Override
    public String toString() {
        return "Policy{" +
                "policyId=" + policyId +
                ", policyName='" + policyName + '\'' +
                '}';
    }
}
