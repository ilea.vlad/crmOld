package com.practicalProject.test.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "reminder")
public class Reminder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reminder_id")
    private int reminderId;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client clientId;

    @Column(name = "message")
    private String message;

    @Column(name = "reminder_date")
    private Date reminderDate;

    public Client getClientId() {
        return clientId;
    }

    public String getMessage() {
        return message;
    }

    public Date getReminderDate() {
        return reminderDate;
    }

    public void setClientId(Client clientId) {
        this.clientId = clientId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setReminderDate(Date reminderDate) {
        this.reminderDate = reminderDate;
    }


    @Override
    public String toString() {
        return "Reminder{" +
                "clientId=" + clientId +
                ", message='" + message + '\'' +
                ", reminderDate=" + reminderDate +
                '}';
    }
}
