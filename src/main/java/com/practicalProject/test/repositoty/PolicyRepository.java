package com.practicalProject.test.repositoty;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.Policy;
import javafx.scene.Scene;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class PolicyRepository {
    private final SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public List<Policy> getAllPolicies() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        List<Policy> policyList = session.createQuery("from Policy").list();

        transaction.commit();;
        session.close();
        return policyList;
    }



    public void addPolicy(Policy policy){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(policy);

        transaction.commit();
        session.close();
    }

    public void updatePolicy(Policy policy){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.update(policy);

        transaction.commit();
        session.close();
    }

    public Policy getPolicyByName(String policyName){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Policy where policyName=:policyName");
        query.setParameter("policyName",policyName);

        Policy policy = (Policy) query.getSingleResult();

        return policy;
    }

}
