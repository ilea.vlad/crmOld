package com.practicalProject.test.repositoty;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.util.CnpToBirthDate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;


import java.util.List;

public class ClientRepository {
    //Session Factory
    private static SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();


    //Crud Operation,methods use Session Transaction

    public List<Client> getAllClients() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        List<Client> clientList = session.createQuery("from Client").list();

        transaction.commit();
        session.close();

        return clientList;
    }

    public List<Client> getClientByCnpOrName(String cnp,String name) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Client where name like :clientName or cnp =:clientCnp");
        query.setParameter("clientName",name + " %");
        query.setParameter("clientCnp", cnp);


        List<Client> clientList = query.list();

        transaction.commit();
        session.close();

        return clientList;
    }


    public Client getClientByName(String name){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Client where name =: clientName");
        query.setParameter("clientName",name);

        Client client = (Client) query.getSingleResult();

        transaction.commit();
        session.close();
        return client;
    }

    public void addClient(Client client){
        Session session = sessionFactory.openSession();
        Transaction transaction =session.beginTransaction();

        session.save(client);
        transaction.commit();
        session.close();
    }

    public void updateClient(Client client) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.update(client);
        transaction.commit();
        session.close();
    }

    public List<Client> getLeadsClient(){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Client where lead = true");
        List<Client> clientList = query.list();

        transaction.commit();
        session.close();
        return clientList;
    }
    public List<Client> getClientBirthday() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        List<Client> clientList = session.createQuery("from Client where gdpr = true").list();
        List<Client> clientWithBirthDay = CnpToBirthDate.CnptoDate(clientList);

        transaction.commit();
        session.close();

        return clientWithBirthDay;
    }



    //Querry the DB using SQL,HQL or Criteria

    //getAllTariners

    //getTrainerById

    //createTainer

    //deleteTrainer

}
