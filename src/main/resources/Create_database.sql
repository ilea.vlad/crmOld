create database crm;
create table client(
client_id int not null auto_increment primary key,
name varchar(50) not null,
address varchar(100),
phone_number varchar(10),
email_address varchar(50),
cnp varchar(13) not null,
gdpr boolean not null
);
create table policy(
policy_id int not null auto_increment primary key,
policy_type varchar(50) not null
);
create table client_policy(
client_id int not null,
policy_id int not null,
foreign key(client_id) references client(client_id) on update cascade on delete cascade,
foreign key(policy_id) references policy(policy_id),
policy_number varchar(50),
period int,
start_date date,
end_date date,
fee decimal 
);
create table reminder(
client_id int not null,
foreign key(client_id) references client(client_id) on update cascade on delete cascade,
message text,
reminder_date date
);
alter table client_policy
add column(client_policy_id int not null auto_increment primary key);
alter table reminder
add column(reminder_id int not null auto_increment primary key);